"use strict"
const browserSync = require("browser-sync");
var gulp = require("gulp"),
    sass = require("gulp-sass");
    browserSync = require("browserSync");
    del=requiere("del");
    imagemin=requiere("gulp-imagemin");
    uglify=requiere("gulp-uglify");
    usemin=requiere("gulp-usemin");
    rev=requiere("gulp-rev");
    cleanCss=requiere("gulp-clean-css");
    flatmap=requiere("gulp-flatmap");
    htmlmin=requiere("gulp-htmlmin");

gulp.task("sass", function(){
    gulp.src("./css/*.scss")
    .pipe(sass().on("error",sass.logError))
    .pipe(gulp.dest("./css"));
})

gulp.task("sass:watch", function(){
    gulp.watch("./css/*.scss",["sass"]);
})

gulp.task("browser-sync", function(){
   var files= ['./*.html','./css/*.css','./images/*.{png,jpg,gif}','./js/*.js']
   browserSync.init(files,{
       server:{
           baseDir:"./"
       }
   })
})

gulp.task("default", ['browser-sync',function(){
    gulp.start("sass:watch");
}]);

gulp.task("copyfonts", function(){
    gulp.src("./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*")
    .pipe(gulp.dest("./dist/fonts"));
});

gulp.task("clean", function(){
    return del([dist]);
});

gulp.task("imagemin", function(){
    return gulp.src("./images/*.{png,jpg,gif,jpeg}")
    .pipe(imagemin({opimizationLevel:3,progressive:true, interlaced:true}))
    .pipe(gulp.dest("dist/images"));
});

gulp.task("usemin",function(){
    return gulp.src("./*.html")
    .pipe(flatmap(function(stream, file){
        return stream
        .pipe(usemin({
            css:[rev()],
            html:[function(){return htmlmin({collapseWhitespace:true})}],
            js:[uglify(),rev()],
            inlinejs:[uglify()],
            inlinecss:[cleanCss(),"concat"]
        }));
    }))
    .pipe(gulp.dest("dist/"));
})

gulp.task("build",["clean"],function(){
    gulp.start("copyfonts","imagenmin","usemin");
})