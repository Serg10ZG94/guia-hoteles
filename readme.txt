Inicio de proyecto
--APUNTES
Git comandos

git --version : Ver que esté instalado bien y versión
git status:  ver si hay cambios por hacer.
git add readme.txt : Agregar archivo al stage
git add . : Supongo para agregar todos los archivos al stage
git reset readme: Quitar archivos del stage
git commit -m 'soyunmensajedelcambioponerversion'
git push: subo los cambios a bitbucket Github.
git init: inicializa git
git remote add origin https://bitbucket.org/Serg10ZG94/guia-hoteles/src/master/ donde se almacena el código de origen
git checkout: me regresa al código anterior al cambio. (deshace cambios) antes del commit
git branch: nos muestra las ramas actuales
git branch login: crea rama login
git checkout login: moverme a rama login
git push -u origin master sube el codigo local a github, bitbucket,etc.
touch -> crear un archivo en vim o linux
git merge nombre de rama: esto permite mezclar la rama actual con la master.
git checkout -b nombreRama: crear nueva rama
git log -p : ver historial
git reset --hard HEAD~2 : deshace los cambios nose
git help rebase:  ayuda de lo que significa rebase
git fecth: update branches
git pull: update and has a merge included 

merge vs rebase:
git merge --squash feature : suma todo lo de los archivos de ramas sin quitar cosas.
git rebase master: 
merge no deja clara la historia espacios, etc. rebase es mejor, la historia da sentido.
Readme.md -> markdown

VCS -> sistema de control de versiones

https://github.com/johnpapa/lite-server -> Servidor orientado a single page app (corre con browsersinc al 
guardar el archivo se refresca solo)

NPM comandos

npm -v : version instalada
npm init: iniciar el npm para crear servidor o archhivo package.json
entrypoint: index.html
npm run dev : levanta el server de node y corre en el puerto 3000 localhost

instalar paquete de bootstrap:
npm install bootstrap --save

dependencias de bootstrap: popper y jquery instalandolas:

npm install jquery --save
npm install popper.js --save
npm install : se baja todo lo de package.json en node_modules
npm uninstall jquery --save : quita la dependencia instalada previamente

Flex comandos

d-flex : inicializar flex en un div
flex-wrap flex-row-reverse: sirve para ordenar en fila al reves del 1234 al 4321
flex-column justify-content-between : sirve para separar el espacio entre 2 objetos uno arriba y otro al fondo espacio.
flex:1 150px : cada elemento se ajusta 1 espacio de 150px.

Semana 2
Navegabilidad navbar, breadcumbs
fixed-top: deja fija la navbar nos sigue en el desplazamiento.
open-iconic

Npm instalar fontawesome
npm install --save @fortawesome/fontawesome-free
npm install node-sass --save-dev (instalamos node con sass)
npm run scss => corre el compilador de sass y lo hace css

SASS  VS LESS
--SASS---
instalar sass:
npm install sass --save 
npm install node-sass  --save-dev *Otra forma integrada a nodejs

Se agrega sass a package.json
"scripts": {
    "dev": "lite-server",
    "test": "echo \"Error: no test specified\" && exit 1",
    "scss":"node-sass -o css/ css/"
  },
  para compilar

compilar sass:
npm run scss

Ejemplos:
$purple:#800080;

/*MIXIN AGRUPA PARTES COMUNES O IMPORTA OTROS ARCHIVOS*/

@mixin margenes($top,$bottom) {
    margin-top: $top;
    margin-bottom:$bottom;
}
--LESS---
instalar less:
npm install -g less

compilar less:
lessc css/styles2.less css/styles2.css

Ejemplo:
@purple:#800080;

/*MIXIN AGRUPA PARTES COMUNES O IMPORTA OTROS ARCHIVOS*/

.margenes(@top:0px, @bottom:0px) {
    margin-top: @top;
    margin-bottom:@bottom;
}

Angular Comandos
ng serve : Lanza la app al localhost
En powershell Set-ExecutionPolicy nos permite ejecutar scripts desde la
consola de vscode, como administrador.
ng g cl models/employee crea carpeta y clase employee en app de angular.

MasterPage con Jquery y HTML5 ver archivo MasterPage.js.

Hacer sitio deployable para producción

Instalamos ONCHANGE RIMRAF
npm install --save rimraf

ONCHANGE= Monitorea cambios en archivos especificos y ejecuta tareas al modificar rutas.
Ejemplo al modificar archivos less o sass los genera compila a css.

CONCURRENCTLY = Permite combinar la ejecucón de varias tareas conjuntamente. Cambios y recarga la pagina automaticamente.

"watch:scss":"onchange 'css/*.scss' -- npm run scss "
Cuando cambie un archivo de sass lo compila
corre 
npm run watch:scss

package.json
"start":"concurrenctly"

 instalación
npm install --save concurrenctly

Mover archivos de carpeta a otra
npm install --save-dev copyfiles

sudo npm install -g imagemin-cli --unsafe-perm-true --allow-root
mejor use: npm install -g imagemin-cli

 "clean": "rimraf dist"
 borrado completo de la carpeta dist recursivo para compilar

 Minificar imagenes:
 "imagemin":"imagemin images/* --out-dir dist/images"
 npm run imagemin


usemin=minificar u ofuzcar.
npm install --save-dev usemin-cli cssmin uglifyjs htmlmin

Preparando para minificar css y js

 <!-- build:js dist/index.js-->
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="js/masterPage.js"></script>
    <script src="js/modalBoton.js"></script>
    <!--endbuild-->

     <!-- build:css dist/index.css-->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="node_modules/open-iconic/font/css/open-iconic-bootstrap.css">
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">
     <!-- endbuild -->


corremos el minificador npm run usemin

Automatizador de tareas.

GULP VS GRUNT

--GRUNT
Instalarlo:
npm install grunt --save-dev

Correrlo:
npm run grunt

Gruntfile.js

module.exports = function (grunt){
    grunt.initConfig({
        sass:{
            dist:{
                files:[{
                    expand:true,
                    cwd:'css',
                    src:['*.scss'],
                    dest:'css',
                    ext:'.css'
                }]
            }
        }
    })

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt-registerTask('css',['sass']);
};


Comando para instalar herramienta de grub para css.
npm install grunt-contrib-sass --save-dev

npm install time-grunt --save-dev
Tiempo y estadisticas de grunt

npm install jit-grunt --save-dev

 npm install grunt-contrib-copy --save-dev
npm install grunt-contrib-clean --save-dev
npm install grunt-contrib-concat --save-dev
npm install grunt-contrib-uglify --save-dev
npm install grunt-contrib-filerev --save-dev
npm install grunt-filerev --save-dev
npm install grunt-usemin --save-dev

GULP
npm isntall -g gulp-cli
npm install gulp --save-dev
npm install gulp-sass --save-dev
npm install browser-sync --save-dev