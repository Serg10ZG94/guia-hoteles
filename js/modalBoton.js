//Comportamiento del modal y del botón
$(function(){
    //Inicializar pop over , tooltip y carrusel configurado.
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval:5000
    });
    //Modal de contacto al mostrarse.
    $("#contacto").on("show.bs.modal",function(e){
        console.log("el modal contacto se está mostrando");
        
    });
    //Modal de contacto al ya estar visible.
    $("#contacto").on("shown.bs.modal",function(e){
        console.log("el modal contacto se mostró");
        $('#contactoBtn').removeClass("btn-success");
        $('#contactoBtn').addClass("btn-primary");
        $('#contactoBtn').prop("disabled",true);
    });
    //Modal de contacto al ocultarse.
    $("#contacto").on("hide.bs.modal",function(e){
        console.log("el modal contacto se está oculta");
    });
    //Modal de contacto al ya estar oculto.
    $("#contacto").on("hidden.bs.modal",function(e){
        console.log("el modal contacto se ocultó");
        $('#contactoBtn').removeClass("btn-primary");
        $('#contactoBtn').addClass("btn-success");
        $('#contactoBtn').prop("disabled",false);
    });
});