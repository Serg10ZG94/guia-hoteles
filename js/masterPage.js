//Ejemplo de MasterPage via Jquery.
$(function() {
    AgregarHeader();
    AgregarFooter();
});

//Aquí va el codigo a cambiar de la barra de navegación superior del sitio.
function AgregarHeader() {
    var header = document.getElementById('Header');
    header.innerHTML='<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">'+
    '<a class="navbar-brand" href="index.html">'+
    'Insignia Diseñadores'+
    '</a>'+
    '<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSuppportedContent" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">'+
    '<span class="navbar-toggler-icon"></span>'+
    '</button>'+
    '<div class="collapse navbar-collapse" id="navbarSuppportedContent">'+
    '<ul class="navbar-nav mr-auto">'+
    '<li class="nav-item active"><a class="nav-link" href="./index.html">Inicio</a></li>'+
    '<li class="nav-item"><a class="nav-link" href="./about.html">Nosotros</a></li>'+
    '<li class="nav-item"><a class="nav-link" href="./precios.html">Precios</a></li>'+
    '<li class="nav-item"><a class="nav-link" href="./contacto.html">Contacto</a></li>'+
    '<li class="nav-item"><a class="nav-link" href="#"> Condiciones <span class="badge badge-light">New</span></a></li>'+
    '<li><a><div><button class="btn btn-success" data-toggle="modal" data-target="#contacto" id="contactoBtn">Registrarme</button></div></a></li>'+
    '</ul>'+
    '</div>'+
    '</nav>';
}

//Aquí va el código a cambiar para el footer del sitio.
function AgregarFooter() {
    var footer = document.getElementById('Footer');
    footer.innerHTML='<footer>'+
    '<div class="row">'+
    '<div class="col-sm-4 d-flex flex-column">'+
    '<a href="https://www.facebook.com/InsigniaDesigners"><i class="fab fa-facebook-f footer-address-icon"></i>Facebook</a>'+
    '<a href="https://www.twitter.com"><i class="fab fa-twitter footer-address-icon"></i>twitter</a>'+
    '<a href="https://www.google.com"><i class="fab fa-google-plus-g footer-address-icon"></i>Google +</a>'+
    '<a href="https://www.instagram.com/@InsigniaDesigners"><i class="fab fa-instagram footer-address-icon"></i>Instagram</a>'+
    '</div>'+
    '<div class="col-sm-4 direccion">'+
    '<address>'+
    '<h3>Oficina Central</h3>'+
    '<p><span class="oi oi-home footer-address-icon"></span>Home Office Temporal, NL, Mexico</p>'+
    '<p><span class="oi oi-phone footer-address-icon"></span>+5281982989212</p>'+
    '<p><i class="fas fa-envelope footer-address-icon"></i>insigniadisenadores@gmail.com</p>'+
    '</address>'+
    '</div>'+
    '<div id="menuInferior" class="col-sm-4 d-flex flex-column align-items-end direccion">'+
    '<a href="./about.html">Nosotros</a>'+
    '<a href="./precios.html">Precios</a>'+
    '<a href="#">Términos y condiciones</a>'+
    '<a href="./contacto.html">Contacto</a>'+
    '</div>'+
    '<div id="menuInferior2" class="col-sm-4 d-flex flex-column align-items-start direccion">'+
    '<a href="./about.html">Nosotros</a>'+
    '<a href="./precios">Precios</a>'+
    '<a href="#">Términos y condiciones</a>'+
    '<a href="./contacto.html">Contacto</a>'+
    '</div>'+
    '</div>'+
    '</footer>';
}